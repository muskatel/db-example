package no.noroff.MessageAPI.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name="MessageUser")
public class User {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String username;

    @JsonGetter("sent")
    public List<String> sent_messages() {
        return sent.stream()
                .map(message -> {
                    return "/message/" + message.id;
                }).collect(Collectors.toList());
    }

    @JsonGetter("received")
    public List<String> received_messages() {
        return received.stream()
                .map(message -> {
                    return "/message/" + message.id;
                }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "Sender",fetch=FetchType.LAZY)
    public Set<Message> sent = new HashSet<Message>();

    @OneToMany(mappedBy = "Receiver",fetch=FetchType.LAZY)
    public Set<Message> received = new HashSet<Message>();

}
