package no.noroff.MessageAPI.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Message
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    @JsonGetter("Sender")
    public String user_sender()
    {
        if(Sender != null)
        {
            return "/user/" + Sender.id;
        }
        else
        {
            return null;
        }
    }

    @ManyToOne()
    @JoinTable(
            name = "Sender",
            joinColumns = { @JoinColumn(name = "message_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    public User Sender;

    @JsonGetter("Receiver")
    public String user_receiver()
    {
        if(Receiver != null) {
            return "/user/" + Receiver.id;
        }
        else
        {
            return null;
        }
    }

    @ManyToOne()
    @JoinTable(
            name = "Receiver",
            joinColumns = { @JoinColumn(name = "message_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    public User Receiver;

    @Column
    public String Message;
}
