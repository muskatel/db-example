package no.noroff.MessageAPI.Respositories;

import no.noroff.MessageAPI.Models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    Message getById(String id);
}
