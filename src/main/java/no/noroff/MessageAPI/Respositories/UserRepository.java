package no.noroff.MessageAPI.Respositories;

import no.noroff.MessageAPI.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User getById(String id);
    User getByUsername(String Username);
}
