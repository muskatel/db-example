package no.noroff.MessageAPI.Controllers;

import no.noroff.MessageAPI.Models.Message;
import no.noroff.MessageAPI.Respositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {

    @Autowired
    private MessageRepository messageRepository;

    //Create    - POST
    @PostMapping("/message")
    public Message addMessage(@RequestBody Message message)
    {
        message = messageRepository.save(message);
        return message;
    }

    //Read      - GET
    @GetMapping("/message/{id}")
    public Message getMessageById(@PathVariable Integer id)
    {
        Message message = null;

        if(messageRepository.existsById(id)) {
            message = messageRepository.findById(id).get();
        }
        return message;
    }

    //Read ALL  - GET
    @GetMapping("/message/all")
    public List<Message> getAllMessages()
    {
       return messageRepository.findAll();
    }

    //Update    - PATCH

    @PatchMapping("/message/{id}")
    public Message updateMessageById(@RequestBody Message message, @PathVariable Integer id)
    {
        Message dbMessage = null;

        if(messageRepository.existsById(id))
        {
            dbMessage = messageRepository.findById(id).get();

            if(message.Sender != null)
            {
                dbMessage.Sender = message.Sender;
            }

            if(message.Receiver != null)
            {
                dbMessage.Receiver = message.Receiver;
            }

            if(message.Message != null)
            {
                dbMessage.Message = message.Message;
            }

            dbMessage = messageRepository.save(dbMessage);

        }
        return dbMessage;
    }
}
