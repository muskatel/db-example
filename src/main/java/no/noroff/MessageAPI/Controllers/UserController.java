package no.noroff.MessageAPI.Controllers;

import no.noroff.MessageAPI.Models.User;
import no.noroff.MessageAPI.Respositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController
{
    @Autowired
    private UserRepository userRepository;

    //Create    - POST
    @PostMapping("/user")
    public User addUser(@RequestBody User user)
    {
        user = userRepository.save(user);
        return user;
    }

    //Read      - GET
    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable Integer id)
    {
        User user = null;

        if(userRepository.existsById(id)) {
            user = userRepository.findById(id).get();
        }
        return user;
    }

    //Update    - PUT

    //Delete    - DELETE
    @DeleteMapping("/user/{id}")
    public String deleteUserById(@PathVariable Integer id)
    {
        String message = "";
        if(userRepository.existsById(id))
        {
            userRepository.deleteById(id);
            message = "User " + id + " was deleted.";
        }
        else
        {
            message = "User " + id + " was not found.";
        }
        return message;
    }
}
